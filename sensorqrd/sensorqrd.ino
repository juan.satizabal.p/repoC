#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif
BluetoothSerial SerialBT;
#include <analogWrite.h>
#define qrd1  26 //sensor 3 //centro
#define qrd2  13 //sensor 1//centro
#define qrd3  32 // sensor 2// centro
#define qrd4  33 // sensor 4 /// izquierda
#define qrd5  14  // sensor 5 /// derecha 
#define led1 2
#define referencia 3500
int ContV =60;
int ContVL =60;
int PWMr=0;
int PWMl=0;
int a = 0,b=0; 
int x; /// variable de   valor  del sesnor 1 
int x2; /// variable de   valor  del sesnor 2 
int x3; /// variable de   valor  del sesnor 3 
int x4; /// variable de   valor  del sesnor 4 
int x5; /// variable de   valor  del sesnor 5 

//mortor izquierdo
const int pwma=27;
const int ain1=32;
const int ain2=33;
//activar  el controlador 
const int stby=14;
//motor derecho
const int pwmb=12;
const int bin1=25;
const int bin2=26;

void setup() {
  pinMode(qrd1,INPUT);
  pinMode(qrd2,INPUT);
  pinMode(qrd3,INPUT);
  pinMode(qrd4,INPUT);
  pinMode(qrd5,INPUT);
  pinMode(pwma,OUTPUT);
  pinMode(ain1,OUTPUT);
  pinMode(ain2,OUTPUT);
  pinMode(stby,OUTPUT);
  pinMode(pwmb,OUTPUT);
  pinMode(bin1,OUTPUT);
  pinMode(bin2,OUTPUT);

  digitalWrite(stby,HIGH); // INICIALIZAR 
  
  Serial.begin(115200); //Configuracion de la velocidad serial
  SerialBT.begin("QRD1114"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
}

void loop() {
      if (PWMr >= ContV ) {
        PWMr = ContV; ////////////////control de velocidad de motores
       }

      if ( PWMl <= ContV ) {
        PWMl = ContV;
      }
      if (PWMl >= ContVL)
      {
        PWMl = ContVL;
      }
      if (PWMr <= ContVL)
      {
        PWMr = ContVL;
      }


  lecturadesensores();
  casos();
      SerialBT.print(x);                                                                                  
      SerialBT.print(";");                                                                                 
      SerialBT.print(x2);
      SerialBT.print(";");
      SerialBT.print(x3);
      SerialBT.print(";");
      SerialBT.print(x4);
      SerialBT.print(";");
      SerialBT.println(x5);
      SerialBT.print(";;;;;;;;");
      SerialBT.println(a);
 /////////////////////////////////////////////////////////////
  delay(200); //retardo de 2 segundos para el envio de datos
}


void lecturadesensores(){
   a = 0; 
   b = 0;
   ///////sensor 1 //////////////////////////
  x=analogRead(qrd1);
//  Serial.print("Lectura 1: ");
  //Serial.println(x); //Imprime el valor de la lectura del canal
  if(x<referencia){
      a =a+1;
    }
  ///////sensor 2 //////////////////////////
  x2=analogRead(qrd2);
  //Serial.print("Lectura 2: ");
  //Serial.println(x2); //Imprime el valor de la lectura del canal
    if(x2<referencia){
      a =a+2;
    }
  ///////sensor 3 //////////////////////////
  x3=analogRead(qrd3);
  //Serial.print("Lectura 3: ");
  //Serial.println(x3); //Imprime el valor de la lectura del canal
    if(x3<referencia){
      a =a+4;
    }
  ///////sensor 4 //////////////////////////
  x4=analogRead(qrd4);
  //Serial.print("Lectura 4: ");
  //Serial.println(x4); //Imprime el valor de la lectura del canal
  if(x4<referencia){
      a =a+6;
    }
  ///////sensor 5 //////////////////////////
  x5=analogRead(qrd5);
  //Serial.print("Lectura 5: ");
  //Serial.println(x5); //Imprime el valor de la lectura del canal
  if(x5<referencia){
      a=a+8;
    }
  }

void adelante(){ //motor izquietdo Y DERECHO /// rueda para  adelante y atras  prendo los low  apago los hing  es contradrio de las ruedas 

  analogWrite(pwma,PWMr);//velocidad de motor
  analogWrite(pwmb,PWMl);//velocidad de motor
}
void izquierda(){ //motor izquietdo Y DERECHO /// esto es para adelante 

  analogWrite(pwma,0);//velocidad de motor 
  analogWrite(pwmb,PWMl);//velocidad de motor
}
void izquierda90(){ //motor izquietdo Y DERECHO /// esto es para adelante 
  analogWrite(pwma,0);//velocidad de motor 
  analogWrite(pwmb,PWMl);//velocidad de motor
}
void derecha(){ //motor izquietdo Y DERECHO
  analogWrite(pwma,PWMr);//velocidad de motor
  analogWrite(pwmb,0);//velocidad de motor
}
void atras(){ //motor izquietdo Y DERECHO

  analogWrite(pwma,PWMr);//velocidad de motor
  analogWrite(pwmb,PWMl);//velocidad de motor
}

void detener(){ //motor izquietdo Y DERECHO

  analogWrite(pwma,0);//velocidad de motor
  analogWrite(pwmb,0);//velocidad de motor
}



void casos(){///casos para que se mantega en la linea  x, x2 y x3
  int contador=0;
  switch(a){
  case 0: ///adelante
    adelante();// 111 o 101 ///cero balnco y negro 1 ///con linea
    break;
  case 1://izq
  if(contador>=30){
   izquierda(); // 90
   contador++;
   
  }
   break;
  case 2://izq 
   izquierda(); //90
   break;
  case 3://izq
    izquierda(); //90
   break;
//  case 4://derecha
//   derecha(); // 110 
//    break;
//  case 5://adelante
//     adelante(); // 101
//    break;
//  case 6:///derecha
//     derecha();//100
    break;
  case 7:// izq
   izquierda(); //90
   break;
  case 8: ///izq
    izquierda();// 90
    break;
  case 9: ///izq
    izquierda();// 90
    break;
  case 10: ///izq
    izquierda();// 90
    break;
  case 11: ///izq
    izquierda();// salio
    break;
//  case 12: ///adelante
//    adelante();// 111 o 101 ///cero balnco y negro 1 ///con linea
//    break;
  case 13: ///izq
    izquierda();// 90
    break;
//  case 14: ///adelante
//    adelante();// 111 o 101 ///cero balnco y negro 1 ///con linea
//    break;
  case 15: //derecha
   derecha();//90
    break;
  case 16: ///derecho
    derecha();// 90
    break; 
  case 17: ///derecha
    derecha();//90
    break;
  case 18: ///derecha
    derecha();// 90
    break;
  case 19: ///adelante
    derecha();// salio
    break;
//  case 20: ///adelante
//    adelante();// 111 o 101 ///cero balnco y negro 1 ///con linea
//    break;
  case 21: ///adelante
    derecha();// 90
    break;
//  case 22: ///adelante
//    adelante();// 111 o 101 ///cero balnco y negro 1 ///con linea
//    break;
  case 23: ///izq
    izquierda();//90
    break;
  case 24: ///adelante
    adelante();// 
    break;
  case 25: ///izq
    izquierda();// salio
    break;
  case 26: ///adelante
    adelante();// 
    break;
  case 27: ///izq
    izquierda();// salio
    break;
  case 28: ///derecho
    derecha();// salio
    break;
  case 29: ///adelante
    adelante();// ?
    break; 
  case 30: ///derecha
    derecha();// salio
    break;
//  case 31: ///adelante
//    adelante();// 111 o 101 ///cero balnco y negro 1 ///con linea
//    break;                                        
  default:
  // detener();//000 //detenido
    break;
    }
  }
